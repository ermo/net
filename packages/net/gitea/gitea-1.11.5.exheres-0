# Copyright 2017-2020 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=go-gitea tag=v${PV} ] \
    systemd-service [ systemd_files=[ contrib/systemd/gitea.service ] ] \
    flag-o-matic

SUMMARY="Gitea - Git with a cup of tea"
HOMEPAGE+=" https://gitea.io"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"

# require additional go packages
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/go[>=1.11]
        dev-lang/node[>=10.0]
    build+run:
        user/gitea
        group/gitea
        sys-libs/pam
    run:
        dev-scm/git[>=1.7.1]
    suggestion:
        dev-db/sqlite:3 [[
            description = [ Required for using a plain SQLite database instead of MySQL or PostgreSQL ]
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-1.11.0-adjust-config.patch
    "${FILES}"/${PN}-1.11.0-adjust-service-file.patch
)

pkg_setup() {
    filter-ldflags -Wl,-O1 -Wl,--as-needed
}

src_prepare() {
    default

    export GOROOT="/usr/$(exhost --target)/lib/go"
    export GOPATH="${WORKBASE}"/build

    edo mkdir -p "${WORKBASE}"/build/src/code.gitea.io
    edo ln -s "${WORK}" "${WORKBASE}"/build/src/code.gitea.io/gitea
}

src_compile() {
    # npm stuff
    esandbox disable_net
    # avoid running into: npm ERR! 429 Too Many Requests
    edo npm config set registry http://registry.npmjs.org/
    emake css js
    esandbox enable_net

    # required to fix build with go >= 1.13, last checked: 1.9.4
    edo export GO111MODULE=off

    edo pushd "${WORKBASE}"/build/src/code.gitea.io/gitea

    edo go fix
    edo go build \
        -o bin/gitea \
        -tags='production sqlite pam cert' \
        -ldflags "${LDFLAGS} -X main.Version=${PV}"

    edo popd
}

src_install() {
    dobin "${WORKBASE}"/build/src/code.gitea.io/gitea/bin/gitea

    insinto /usr/share/gitea
    doins -r {public,templates}
    insinto /usr/share/gitea/options
    doins -r options/{gitignore,label,license,readme}

    insinto /etc/gitea
    newins custom/conf/app.ini.sample app.ini
    edo chown -R gitea:gitea "${IMAGE}"/etc/gitea

    keepdir /var/{lib,log}/gitea
    keepdir /var/lib/gitea/custom

    insinto /var/lib/gitea/conf
    doins -r options/locale

    edo chown gitea:gitea "${IMAGE}"/var/{lib,log}/gitea

    install_systemd_files
}

