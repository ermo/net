(
    dev-libs/tevent[~scm]
    media-video/rtmpdump[~scm]
    net-analyzer/fail2ban[~scm]
    net-fs/samba[~scm]
    net-im/bitlbee[~scm]
    net-im/bitlbee-steam[~scm]
    net-im/minbif[~scm]
    net-irc/weechat[~scm]
    net-irc/znc[~scm]
    net-libs/jreen[~scm]
    net-libs/libndp[~scm]
    net-libs/miniupnpc[~scm]
    net-libs/nghttp3[~scm]
    net-libs/ngtcp2[~scm]
    net-misc/connman[~scm]
    net-misc/mosh[~scm]
    net-p2p/transmission[~scm]
    net-www/uzbl[~scm]
) [[
    *author = [ Exherbo developers ]
    *token = scm
    *description = [ Mask scm versions ]
]]

net-fs/cifs-utils[<=5.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Apr 2012 ]
    token = security
    description = [ CVE-2012-1586 ]
]]

net-misc/tor[<0.2.2.39] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 20 Sep 2012 ]
    token = security
    description = [ Several security problems ]
]]

web-apps/cgit[<0.9.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 30 May 2013 ]
    token = security
    description = [ CVE-2013-2117 ]
]]

net-libs/libssh[<0.9.4] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 09 Apr 2020 ]
    token = security
    description = [ CVE-2020-1730 ]
]]

www-servers/nginx[<1.16.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Mar 2020 ]
    token = security
    description = [ CVE-2019-9511, CVE-2019-9513, CVE-2019-9516 ]
]]

net-fs/openafs[~scm] [[
    author = [ Dirk Heinrichs <dirk.heinrichs@altum.de> ]
    date = [ 11 May 2013 ]
    token = scm
    description = [ Masked SCM version ]
]]

net-libs/libmicrohttpd[<0.9.32] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 21 Dec 2013 ]
    token = security
    description = [ CVE-2013-703{8,9} ]
]]

net-proxy/torsocks[~scm] [[
    author = [ Nicolas Braud-Santoni <nicolas+exherbo@braud-santoni.eu> ]
    date = [ 28 Nov 2013 ]
    token = scm
    description = [ This is a scm version for the rewrite of torsocks ]
]]

net-proxy/torsocks[<2] [[
    author = [ Nicolas Braud-Santoni <nicolas+exherbo@braud-santoni.eu> ]
    date = [ 19 Jan 2013 ]
    token = security
    description = [ See https://lists.torproject.org/pipermail/tor-dev/2013-June/004959.html ]
]]

net-remote/FreeRDP[<2.1.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 26 May 2020 ]
    token = security
    description = [ CVE-2020-1339{6,7,8} ]
]]

app-crypt/krb5[<1.16-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Mar 2018 ]
    token = security
    description = [ CVE-2018-5729, CVE-2018-5730 ]
]]

net-proxy/squid[<4.8] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 18 Jul 2019 ]
    token = security
    description = [ CVE-2019-12525, CVE-2019-12527, CVE-2019-12529, CVE-2019-13345 ]
]]

net-misc/openvpn[<2.4.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Jun 2017 ]
    token = security
    description = [ CVE-2017-7508, CVE-2017-7520 CVE-2017-7521, CVE-2017-7522 ]
]]

net-analyzer/tcpdump[<4.9.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 07 Oct 2019 ]
    token = security
    description = [ Multiple CVEs ]
]]

net-analyzer/wireshark[<3.2.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 26 May 2020 ]
    token = security
    description = [ wnpa-sec-2020-08 ]
]]

net/net-snmp[<5.7.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 15 Jul 2015 ]
    token = security
    description = [ CVE-2014-3565 ]
]]

web-apps/cgit[<0.12] [[
    author = [ Kylie McClain <somasis@exherbo.org> ]
    date = [ 14 Jan 2015 ]
    token = security
    description = [ CVE-2016-1899, CVE-2016-1900, CVE-2016-1901 ]
]]

net-misc/socat[<1.7.3.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 01 Feb 2015 ]
    token = security
    description = [ http://www.dest-unreach.org/socat/contrib/socat-secadv7.html
                    http://www.dest-unreach.org/socat/contrib/socat-secadv8.html ]
]]

(
    dev-db/mariadb[<10.2.32]
    dev-db/mariadb[>=10.3&<10.3.23]
    dev-db/mariadb[>=10.4&<10.4.13]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 26 May 2020 ]
    *token = security
    *description = [ CVE-2020-2752, CVE-2020-2760, CVE-2020-2812, CVE-2020-2814 ]
]]

net-fs/samba[<4.12.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 28 Apr 2020 ]
    token = security
    description = [ CVE-2020-10700, CVE-2020-10704 ]
]]

net-mail/dovecot[<2.3.10.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 18 May 2020 ]
    token = security
    description = [ CVE-2020-10957, CVE-2020-10958, CVE-2020-10967 ]
]]

www-servers/apache[<2.4.43] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 07 Apr ]
    token = security
    description = [ CVE-2020-1934, CVE-2020-1927 ]
]]

dev-scm/libgit2[<0.27.5] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 06 Oct 2018 ]
    token = security
    description = [ CVE-2018-17456 ]
]]

net-irc/weechat[<1.9.1] [[
    author = [ Johannes Nixdorf <mixi@exherbo.org> ]
    date = [ 30 Sep 2017 ]
    token = security
    description = [ CVE-2017-14727 ]
]]

media-video/rtmpdump[<2.4_p20151223] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 May 2017 ]
    token = security
    description = [ CVE-2015-827{0,1,2} ]
]]

net-mail/tnef[<1.4.15] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 29 Sep 2017 ]
    token = security
    description = [ CVE-2017-{6307,6308,6309,6310,8911} ]
]]

net-wireless/hostapd[<2.6] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 16 Oct 2017 ]
    token = security
    description = [ CVE-2017-130{77,78,79,80,81,82,86,87,88} ]
]]

net-remote/teamviewer[<13.2.13582] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Aug 2018 ]
    token = security
    description = [ CVE-2018-143333 ]
]]

sys-auth/sssd[<1.16.1-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 Aug 2018 ]
    token = security
    description = [ CVE-2018-10852 ]
]]

net/mosquitto[<1.6.6] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 30 Oct 2019 ]
    token = security
    description = [ CVE-2019-11779 ]
]]

net-libs/nghttp2[<1.39.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 14 Aug 2019 ]
    token = security
    description = [ CVE-2019-9511, CVE-2019-9513 ]
]]

net-irc/znc[<1.7.3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 01 Apr 2019 ]
    token = security
    description = [ CVE-2019-9917 ]
]]

net-p2p/transmission[<2.94] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 19 Jul 2018 ]
    token = security
    description = [ CVE-2018-5702 ]
]]

net-analyzer/ettercap[<0.8.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Jan 2019 ]
    token = security
    description = [ CVE-2014-{6395,6396,9376,9377,9378,9379,9380,9381}, CVE-2017-6430 ]
]]

net/synapse[<0.34.1.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 10 Jan 2019 ]
    token = security
    description = [ CVE-2019-5885 ]
]]

net-libs/zeromq[>=4.2.0&<4.3.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 07 Feb 2019 ]
    token = security
    description = [ CVE-2019-6250 ]
]]

net/gitea[<1.7.6] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 15 Apr 2019 ]
    token = security
    description = [ CVE-2019-11228, CVE-2019-11229 ]
]]

net/gogs[<0.11.91] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 18 Nov 2019 ]
    token = security
    description = [ CVE-2019-14544 ]
]]

www-servers/lighttpd[<1.4.54] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 19 Nov 2019 ]
    token = security
    description = [ CVE-2019-11072 ]
]]

sys-cluster/ceph[<14.2.7] [[
    author = [ Arnaud Lefebvre <a.lefebvre@outlook.fr> ]
    date = [ 01 Feb 2020 ]
    token = security
    description = [ CVE-2020-1699, CVE-2020-1700 ]
]]

dev-db/redis[<5.0.8] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Apr 2020 ]
    token = security
    description = [ CVE-2015-8080 ]
]]

www-servers/tomcat-bin[<8.5.51] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 Feb 2020 ]
    token = security
    description = [ CVE-2019-17569, CVE-2020-1935, CVE-2020-1938 ]
]]

dev-db/mariadb[>10.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 17 Feb 2020 ]
    token = testing
    description = [ Give it a bit of time to make sure nothing major breaks ]
]]

net/coturn[<4.5.1.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 08 May 2020 ]
    token = security
    description = [ CVE-2020-6061, CVE-2020-6062 ]
]]

web-apps/mailman[<2.1.33] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 11 May 2020 ]
    token = security
    description = [ CVE-2020-12137 ]
]]
